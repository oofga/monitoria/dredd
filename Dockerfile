FROM python:3.6

RUN apt-get update && apt-get install vim git -y

ADD ./requirements.txt /tmp/requirements.txt

RUN pip install -r /tmp/requirements.txt

ADD ./apps/ /dredd/apps

WORKDIR dredd/

CMD python /dredd/apps/main.py
