import requests
import datetime

from report import Report


class Analyser:
    def __init__(self, mainEpUrl, commitsEpurl, monitorEmails):
        self.mainEpUrl = mainEpUrl
        self.commitsEpurl = commitsEpurl
        self.monitorEmails = monitorEmails

    def analyse_project(self, project, debug=False):
        projectCommits = self.get_projects_commits(project['id'])
        if debug:
            print('Student name:',project['name_with_namespace'].split('/')[0])
            print('Student repository:',project['web_url'])
            print('Project commits:', projectCommits[0])
            print('Project creation:', self.transform_to_datetime(project['created_at']))
            print('First commit:', projectCommits[1])
            print('Last commit:', projectCommits[2])
            print('Days worked:', projectCommits[3])
            print('Project authors:', projectCommits[4])
            print('Project committers:', projectCommits[5])
            print('-'*50)
        report = Report(self)
        report.add_report(project, projectCommits)
    
    def get_projects_commits(self, projectId):
        request = requests.get(self.commitsEpurl.format(projectId))
        projectCommits = request.json()
        commitsData = []
        commitsAmount=0
        
        # Get student commits amount
        for commit in projectCommits:
            if commit['author_email'] not in self.monitorEmails :
                commitsAmount+=1
        commitsData.append(commitsAmount)
    
        # Verify if student made something
        if commitsAmount != 0:
    
            # Get first student commit
            projectCommits.reverse()
            for commit in projectCommits:
                if commit['author_email'] not in self.monitorEmails:
                    commitsData.append(self.transform_to_datetime(commit['created_at']))
                    break
    
            # Get last student commit
            projectCommits.reverse()
            commitsData.append(self.transform_to_datetime(projectCommits[0]['created_at']))
    
            # Get worked days
            commitsData.append(self.calculate_worked_days(commitsData[1], commitsData[2]))
    
        else :
            # If student just made nothing
            commitsData.append('None')
            commitsData.append('None')
            commitsData.append('None')
    
        # Get authors emails
        author_email = []
        for commit in projectCommits:
            if commit['author_email'] not in self.monitorEmails:
                author_email.append(commit['author_email'])
        commitsData.append(str(set(author_email)).strip('{}'))
    
        # Get committers emails
        committer_email = []
        for commit in projectCommits:
            if commit['committer_email'] not in self.monitorEmails:
                committer_email.append(commit['committer_email'])
        commitsData.append(str(set(committer_email)).strip('{}'))
    
        return commitsData
    
    def transform_to_datetime(self, data):
        data = data.split('T')[0] + '-' + data.split('T')[1].split('.')[0]
        return datetime.datetime.strptime(data, '%Y-%m-%d-%H:%M:%S')
    
    def calculate_worked_days(self, firstDay, lastDay):
        return (lastDay - firstDay).days
