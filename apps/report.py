class Report:
    def __init__(self, analyser):
        self.analyser = analyser

    def generate_report(self):
        with open('report.md', 'w') as file:
            file.write('# Dredd Report' + '\n\n')
        
    def add_report(self, project, projectCommits):
        with open('report.md', 'a') as file:
            file.write('### Student name: ' + project['name_with_namespace'].split('/')[0] + '\n\n')
            file.write('* Student repository: ' + '[' + project['path_with_namespace'] + '](' + project['web_url'] + ')\n\n')
            file.write('* Project commits: ' + str(projectCommits[0]) +  '\n\n')
            file.write('* Project creation: ' + str(self.analyser.transform_to_datetime(project['created_at'])) + '\n\n')
            file.write('* First commit: ' + str(projectCommits[1]) + '\n\n')
            file.write('* Last commit: ' + str(projectCommits[2]) + '\n\n')
            file.write('* Days worked: ' + str(projectCommits[3]) + '\n\n')
            file.write('* Project authors: ' + str(projectCommits[4]) + '\n\n')
            file.write('* Project committers: ' + str(projectCommits[5]) + '\n\n')
            file.write('\n')
    
    def write_in_report(self, data):
        with open('report.md', 'a') as file:
            file.write(data+'\n')
