import requests
import subprocess
import os
import progressbar
import sys
import time

from report import Report
from analyser import Analyser
from messages import Messages


class Dredd:
    def __init__(self, mainRepositoryUrl, mainRepositoryId, commitsEpurl, monitorEmails):
        self.mainRepositoryUrl = mainRepositoryUrl
        self.mainRepositoryId = mainRepositoryId
        self.commitsEpurl = commitsEpurl
        self.monitorEmails = monitorEmails
        self.messages = Messages()
    
    def find_forked_projects(self):
        request = requests.get(self.mainRepositoryUrl.format(self.mainRepositoryId))
        if request.ok:
            forkedProjects = request.json()
            return forkedProjects
        else:
            self.messages.dredd_helmet(message=' '*6+'PROBLEM WHILE GETING FORKS!!!')
            sys.exit()
    
    def analyse_and_get_data(self, forkedProjects):
        studentNames = []
        studentRepositories = []
        self.messages.dredd_helmet(' '*12+'STARTING ANALYSIS!')
        analyser = Analyser(self.mainRepositoryUrl, self.commitsEpurl, self.monitorEmails)
        for project in progressbar.progressbar(forkedProjects):
            analyser.analyse_project(project)
            studentNames.append(project['name_with_namespace'].split('/')[0])
            studentRepositories.append(project['web_url'])
    
        return [studentNames, studentRepositories]
    
    def clone_each_project(self, studentNames, studentRepositories, projectsDirectory='eps/'):
        try:
            os.mkdir(projectsDirectory)
        except FileExistsError:
            self.messages.dredd_helmet(' '*2+'PROJECTS DIRECTORY ALREDY CREATED!', clearScreen=False, delayTime=0)
        for i in progressbar.progressbar(range(len(studentRepositories))):
            studentNames[i] = projectsDirectory+studentNames[i]
            try:
                output = subprocess.check_output(["git", "clone", '-q', studentRepositories[i], studentNames[i]])
            except subprocess.CalledProcessError:
                self.messages.dredd_helmet('PROJECT FROM {} ALREDY CLONED!'.format(studentNames[i].split('/')[1]),
                                           delayTime=0)
