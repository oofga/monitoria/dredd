import time
import os

from report import Report 


class Messages:
    def __init__(self):
        pass

    def dredd_breastplate(serlf, message1=' '*11, message2=' '*10,
                          delayTime=1.5, writeReport=False, clearScreen=True):
        if clearScreen:
            os.system('cls' if os.name == 'nt' else 'clear')
        dreddBreastplate = ('''
                       ,^^~~~~~~^^~-.,
                     ,^        -,____ ^,         ,/\/\/\,
                    /           (____)  |      S~        ~7
                   ;  .---._    | | || _|     S{}Z
                   | |      ~-.,\ | |!/ |     /_{}_\\
                   ( |    ~<-.,_^\|_7^ ,|     _//_      _\\
                   | |      ", 77>   (T/|   _/'   \/\/\/
                   |  \_      )/<,/^\)i(|
                   (    ^~-,  |________||
                   ^!,_    / /, ,'^~^',!!_,..---.
                    \_ "-./ /   (-~^~-))' =,__,..>-,
                      ^-,__/#w,_  '^' /~-,_/^\      )
                   /\  ( <_    ^~~--T^ ~=, \  \_,-=~^\\
      .-==,    _,=^_,.-"_  ^~*.(_  /_)    \ \,=\      )
     /-~;  \,-~ .-~  _,/ \    ___[8]_      \ T_),--~^^)
       _/   \,,..==~^_,.=,\   _.-~O   ~     \_\_\_,.-=)
     ,(       _,.-<~^\  \ \\      ()  .=~^^~=. \_\_,./
    ,( ^T^ _ /  \  \  \  \ \)    [|   \oDREDD >
      ^T~ ^ ( \  \ _\.-|=-T~\\    () ()\<||>,' )
       +     \ |=~T  !       Y    [|()  \ ,'  / -naughty
       ''')
    
        print(dreddBreastplate.format(message1,message2))
        if writeReport:
            report = Report(None)
            dreddBreastplate= '\n```\n' + dreddBreastplate + '\n```\n'
            report.write_in_report(dreddBreastplate.format(message1,message2))
        time.sleep(delayTime)
    
    def dredd_helmet(self, message='', delayTime=1, writeReport=False,
                     clearScreen=True):
        if clearScreen:
            os.system('cls' if os.name == 'nt' else 'clear')
        dreddHelmet= ('''
               _.--'~~~~~~~~~'--._
            .-'                   '-.
          .'                         '.
        .'                             '.
      .'          |           |          '.
     /            |___________|            \\
    |  /\         |  .  .  .  |         /\  |
    | /  '-.      |___________|      .-'  \ |
    |/  ,.  \     |   |   |   |     /  ..  \|
    |\  \ \  '.   |   |   |   |   .'  / /  /|
    | \  \ '.  \  |   |   |   |  /  .' /  / |
    |  \  |  \  '.|   |   |   |.'  /  |  /  |
    |   | |   '.  \   |   |   /  .'   | |   |
    |   | |     \  '-.|   |.-'  /     | |   |
    |   | |      '-.  '._.'  .-'      | |   |
    |   | |         \       /         | |   |
    |   | |          \_____/          | |   |
    |   | |'.                       .'| |   |
    |   | |  '.                   .'  | |   |
    |   | |'.  '-._           _.-'  .'| |   |
    |   | |  '.    '-._____.-'    .'  | |   |
     \ /   \   '-._           _.-'   /   \ /
      \|    \      '-._____.-'      /    |/
       '.    \          _ _        /    .'
         \   ||      ________     ||   /
          '. ||     /        \    || .'
            \//\       --  -     /\\/
                |               |
                |               |
                 '.___________.'
    
    {}
        ''')
        print(dreddHelmet.format(message))
        if writeReport:
            report = Report(None)
            dreddHelmet = '\n```\n' + dreddHelmet + '\n```\n'
            report.write_in_report(dreddHelmet.format(message))
        time.sleep(delayTime)
