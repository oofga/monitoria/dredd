from dredd import Dredd
from messages import Messages
from analyser import Analyser
from report import Report
from plagiarism import Plagiarism

MAIN_REPOSITORY_URL = 'https://gitlab.com/api/v4/projects/{}/forks?per_page=100'
MAIN_REPOSITORY_ID = '8267093'
COMMITS_EP_URL = 'https://gitlab.com/api/v4/projects/{}/repository/commits?per_page=100'
MONITORS_EMAIL = 'arthurrtl@gmail.com'
PROJECTS_DIRECTORY ='eps/'
IGNORATED = ['using namespace std;\n','#include <vector>\n',
             '#include <iostream>\n','};\n','}\n','\n','#include <fstream>\n',
             'int main() {\n','#include <string>\n','#include <string>\n','}',
             '{']

if __name__ == '__main__':
    dredd = Dredd(MAIN_REPOSITORY_URL,MAIN_REPOSITORY_ID,COMMITS_EP_URL,MONITORS_EMAIL)
    analyser = Analyser(MAIN_REPOSITORY_URL.format(MAIN_REPOSITORY_ID),
                        COMMITS_EP_URL.format(MAIN_REPOSITORY_ID),
                        MONITORS_EMAIL)
    report = Report(analyser)
    messages = Messages()
    report.generate_report()
    messages.dredd_breastplate('  I AM THE  ', '   LAW!   ', writeReport=True)
    analyser.analyse_data = dredd.analyse_and_get_data(dredd.find_forked_projects())
    messages.dredd_helmet(' '*4+'ANALYSIS FINISHED! STARTING CLONING!')
    dredd.clone_each_project(analyser.analyse_data[0],analyser.analyse_data[1])
    messages.dredd_helmet('CLONING FINISHED! STARTING PLAGIARISM FINDER!')
    plagiarism = Plagiarism(PROJECTS_DIRECTORY, IGNORATED)
    plagiarism.print_plagiarism_report()
    messages.dredd_helmet(' '*14+'JUDGMENT DONE', writeReport=True)
