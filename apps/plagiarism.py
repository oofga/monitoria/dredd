import os
import progressbar


class Plagiarism:
    reports=[]
    count=0
    def __init__(self, projectsDirectory, ignorated):
        self.projectsDirectory = projectsDirectory
        self.ignorated = ignorated

    def find_files(self):
        files_name=[]
        for root, dirs, files in os.walk(self.projectsDirectory, topdown=False):
            for file_name in files:
                if '.cpp' in file_name or '.hpp' in file_name:
                    if '.swp' not in file_name and '.gch' not in file_name and '~' not in file_name:
                        #print(os.path.join(root, file_name))
                        files_name.append(os.path.join(root, file_name))
        return files_name
    
    def compare(self, files_name):
        for i in progressbar.progressbar(range(len(files_name))):
            with open(files_name[i], 'r') as project_file:
                for j in range(i+1, len(files_name)):
                    if files_name[i].split('/')[1] not in files_name[j].split('/')[1]:
                        with open(files_name[j], 'r') as new_project_file:
                            self.count=0
                            try:
                                comparison = set(project_file).intersection(new_project_file)
                                for item in self.ignorated:
                                    comparison.discard(item)
                                if comparison != set():
                                    self.reports.append([files_name[i].split('/')[1],
                                                   files_name[j].split('/')[1],
                                                   comparison,
                                                   project_file.name.split('/')[-1],
                                                   new_project_file.name.split('/')[-1]
                                                   ])
                            except UnicodeDecodeError:
                                print(project_file)
                                print(new_project_file)
                                print('Problem while compairing')
    
    def print_plagiarism_report(self):
        self.compare(self.find_files())
        with open ('plagiarism.md','w') as plagiarism:
            plagiarism.write('# Plagiarism found\n\n')
            for report in self.reports:
                plagiarism.write('In file: `'+report[3]+'` from **'+report[0].strip()+'**\n\n')
                plagiarism.write('Equals: `'+report[4]+'` from **'+report[1].strip()+'**\n\n') 
                for equals in report[2]:
                    plagiarism.write('Line: \n\n```\n'+equals+'```\n\n')
                plagiarism.write('-'*20+'\n\n')
