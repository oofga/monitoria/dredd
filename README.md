# Dredd

An application that assists the evaluation process of the practical exercises.

## Instalation

* Install the requirements

`pip install -r requirements.txt`

* Create the docker container

`docker build -t dredd .`

## Usage

* Run the Dredd localy

`python apps/main.py`

* Run using docker

`sudo docker run -v $(pwd):/dredd/ -t dredd`

## Report

* After running **Dredd** a report with name `report.md` will be generated.

* After running **Dredd** a plagiarisms report with name `plagiarism.md` will be generated.

## References

* [Dredd ASCII Art](https://www.asciiart.eu/comics/judge-dredd)
* [Gitlab API](https://docs.gitlab.com/ee/api/)
* [Gitlab forks](https://docs.gitlab.com/ee/api/projects.html#list-forks-of-a-project)
* [Gitlab projects api](https://docs.gitlab.com/ee/api/projects.html)
* [Gitlab commits api](https://docs.gitlab.com/ee/api/commits.html)
* [Gitlab pagination](https://docs.gitlab.com/ee/api/README.html#pagination)
